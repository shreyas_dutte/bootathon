 
 #               NEWTONS LAW OF COOLING
 
 
 # Aim
 
<ol>
<li>
The aim of the experiment is to verify Newton's Law of Cooling of different materials and different liquids.</li>
<li>
To draw the cooling curve.
</li>

# Theory
 

 

Temperature difference in any situation results from energy flow into a system or energy flow from a system to surroundings. The former leads to heating, whereas latter leads to cooling of an object.
Newton’s Law of Cooling states that the rate of temperature of the body is proportional to the difference between the temperature of the body and that of the surrounding medium. This statement leads to the classic equation of exponential decline over time which can be applied to many phenomena in science and engineering, including the discharge of a capacitor and the decay in radioactivity.<br>

Newton's Law of Cooling is useful for studying water heating because it can tell us how fast the hot water in pipes cools off.  A practical application is that it can tell us how fast a water heater cools down if you turn off the breaker when you go on vacation. <br>

Suppose that a body with initial temperature T1°C, is allowed to cool in air which is maintained at a constant temperature T2°C. <br>
Let the temperature of the body be T°C at time t.

Then by Newton’s Law of Cooling .<br>
 &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
![img](images/newton3.jpg) (1)
<br>

Where k is a positive proportionality constant. Since the temperature of the body is higher than the temperature of the surroundings then T-T2 is positive. Also the temperature of the body is decreasing i.e. it is cooling down and rate of change of temperature is negative. 

&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
![img1](images/newton4.jpg)

The constant ‘k’ depends upon the surface properties of the material being cooled.<br>
Initial condition is given by T=T1 at t=0 <br>
Solving (1)<br>

&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
![img3](images/newton5.jpg)<br>


&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
![img4](images/newton6.jpg) (2)

Applying initial conditions;
<br>
&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
![img5](images/newton7.jpg); <br>

Substituting the value of C in equation (2) gives

&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
![img6](images/newton8.jpg)

his equation represents Newton’s law of cooling.


If k <0, lim t --> ∞, <sub>e</sub><sup>-kt</sup> = 0 and T= T2 ,


Or we can say that the temperature of the body approaches that of its surroundings as time goes.

 


The graph drawn between the temperature of the body and time is known as cooling curve. The slope of the tangent to the curve at any point gives the rate of fall of temperature.
<br>


![img8 ](images/newton2.jpg)

 In general,<br>
 

 T(t) = <sup>T</sup><sub>A</sub> + (<sup>T</sup><sub>h</sub> - <sup>T</sup><sub>A</sub>)<sub>e</sub><sup>-kt</sup>

 where,

 

T(t) = Temperature at time t,<br>
TA = Ambient temperature (temp of surroundings),<br>
TH = Temperature of hot object at time 0,<br>
k = positive constant and <br>
t = time. <br>


# Procedure
<br>

## Materials Required:
<ul>

<li>Copper calorimeter 
<li>Stirrer
<li>Wooden box with a clamp and stand
<li>A wooden lid having a hole in the middle.
<li>Thermometer
<li>Stop watch
<li>Hot water of about 8

## Lab Procedure:
<ul>
<li>Fill about 2/3rd of the copper calorimeter containing stirrer with hot water of about 80 °C.
<li>Place the calorimeter inside the wooden box. The space between the wooden box and calorimeter is filled with cotton to avoid heat loss.
<li>Close the wooden box with its lid.
<li>Suspend the thermometer inside the hot water in the calorimeter from the clamp and stand.
<li>Stir water continuously to make it cool uniformly.
<li>When the temperature of hot water falls to 70°C, start the stop watch.
<li>Note the temperature reading at every five minutes.
Continue the time temperature observation till the temperature becomes constant.
<li>Plot a graph between time along X-axis and temperature along Y-axis. This graph is called the cooling curve.
<li>The graph is an exponential curve and it shows that the temperature falls quickly at the beginning and then slowly as the difference of temperature goes on decreasing. This verifies the Newton’s Law of cooling.

# Observation

|sl no|Time for cooling|Tempertature of water|Temperature of water in enclosure|Difference of temperature|
|---|---|---|---|---|
|1|0|
|2|1|
|3|2|
|4|3|
|5|4|
|6|5|
|7|6|
|8|7|
|9|8|
||

# Calculations:
<ul>
<li>Temperature of water in the enclosure is found to be a constant. If not, then take its mean as T0.
<li>From the observations, we can calculate the temperature difference (T-T0).
<li>Now, plot a graph between time (t) and temperature (T), taking time along X axis and temperature along Y axis. 

![imgy](images/sh.jpg)

# Results:
The cooling curve of the liquid is plotted.<br>

The temperature falls quickly in the beginning and then slowly as the difference of temperature goes on decreasing.
<br>
This is in agreement with Newton’s Law of cooling.<br>
